﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest : MonoBehaviour {

    public string name; //Название квеста
    public string description; //Описание квеста
    public int credits; //Кредиты за выполнение
    public float exp; //Опыт за выполнение
    
    //public Vector3 target; //Цель квеста

    public struct QuestTarget
    {
        long id;
        Vector3 pos;
        int amount;
        Vector3 destination;
    }

    public QuestTarget target;

    public Vector3 takePlace; //Место взятия квеста
    public bool isDone; //Выполнен ли квест

    public Quest(
        string qName,
        string qDescription,
        int qCredits,
        float qExp,
        QuestTarget qTarget,
        Vector3 qTakePlace,
        bool qIsDone)
    {
        name = qName;
        description = qDescription;
        credits = qCredits;
        exp = qExp;
        target = qTarget;
        takePlace = qTakePlace;
        isDone = qIsDone;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}


//Исследовательский
/*
 * класс исслКвест : квест Х
 *  
 *   таргет - айдишник объекта, который нужно исследовать
 *   
 * метод проверки выполнения квеста
 * Х
 *    здесь будет проверяться объект который нужно исследоваться
 *    на существование
 *    
 *    здесь проверка на нахождение айдишника этого объекта в списке исследованных
 *    
 *    если обе проверки трушные то квест выполнен
 * Ъ
 *
 * Ъ
 */

//Оборона
/*
 * если цель существует и если коорды цели прибыли в target.destination
 * то квест выполнен
 */

//уничтожение
/*
 * если цель не существует и цель есть в киллоге
 * то квест выполнен
 */

//добыча
/*
 * если в инвенторе есть target.id в количестве target.amount 
 * то квест выполнен
 */