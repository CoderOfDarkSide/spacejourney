﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll : MonoBehaviour {

	public float startSize = 5.0f;
	public float maxSize = 50.0f;
	public float minimapRenderSize = 30.0f;

	//private Starfield starfield;

	Camera cam;
	float scale;
	int oldMask;

	// Use this for initialization
	void Start () {
		//starfield = GetComponentInChildren<Starfield>();
		cam = GetComponent<Camera>();
		scale = startSize;
		oldMask = cam.cullingMask;
	}
	
	void ResizeCam()
	{
		if (scale >= startSize && scale <= maxSize)
		{
			scale += -Input.GetAxis("Mouse ScrollWheel");

			//starfield.mediumStarsDistance = scale * 2500 / 10;
			//starfield.bigStarsDistance = scale * 1500 / 10;
		}
		else if (scale < startSize)
		{
			scale = startSize;

			//starfield.mediumStarsDistance = scale * 2500 / 10;
			//starfield.bigStarsDistance = scale * 1500 / 10;
		}
		else if (scale > maxSize)
		{
			scale = maxSize;

			//starfield.mediumStarsDistance = scale * 2500 / 10;
			//starfield.bigStarsDistance = scale * 1500 / 10;
		}

		if (scale >= minimapRenderSize)
		{
			cam.cullingMask = 1 << 8;
		}
		else
		{
			cam.cullingMask = oldMask;
		}

		cam.orthographicSize = scale;
	}

	// Update is called once per frame
	void Update ()
	{
		ResizeCam();
	}
}
