﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class EnableGui : NetworkBehaviour {

	[SyncVar]
	public string playerRoleName = string.Empty;
	public GameObject gui;
	Transform canvas;

	GameObject plGameObject;

	void OnGUI()
	{
		canvas = GameObject.Find("IngameCanvas").gameObject.transform;
		if (isLocalPlayer)
		{
			switch (playerRoleName)
			{
				case "Capitan":
					gui = canvas.Find("CapitanPanel").gameObject;
					gui.SetActive(true);
					//GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "This is a Capitan");
					break;
				case "Pilot":
					gui = canvas.Find("PilotPanel").gameObject;
					gui.SetActive(true);
					//GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "This is a Pilot");
					break;
				case "Engeneer":
					gui = canvas.Find("EngeneerPanel").gameObject;
					gui.SetActive(true);
					//GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "This is a Engeneer");
					break;
				case "Scientist":
					gui = canvas.Find("ScientistPanel").gameObject;
					gui.SetActive(true);
					//GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "This is a Scientist");
					break;
				case "Weapons":
					gui = canvas.Find("WeaponsPanel").gameObject;
					gui.SetActive(true);
					//GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "This is a Weapons");
					break;
				default:
					//GUI.Box(new Rect(0, 0, Screen.width, Screen.height), "This is a watcher");
					break;
			}
		}
	}

	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update ()
	{

	}
}
