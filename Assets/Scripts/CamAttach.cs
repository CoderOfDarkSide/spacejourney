﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamAttach : MonoBehaviour {

	public GameObject Ship;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Ship == null)
		{
			Ship = GameObject.Find("Ship");
		}
		else
		{
			Vector3 pos = Vector3.zero;
			pos.x = Ship.transform.position.x;
			pos.y = Ship.transform.position.y;
			pos.z = transform.position.z;
			transform.position = pos;
			pos.x = pos.x * 0.9F;
			pos.y = pos.y * 0.9F;
		}
	}
}
