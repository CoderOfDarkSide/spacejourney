﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowPlanetInfo : MonoBehaviour {

	public GameObject infoPanel;

	// Use this for initialization
	void Start ()
	{
		
	}

	void OnMouseDown()
	{
		Debug.Log("User clicked on plenet!");

		infoPanel.SetActive(!infoPanel.activeSelf);
	}

	// Update is called once per frame
	void Update ()
    {
        infoPanel.transform.rotation = Quaternion.identity;
	}
}
