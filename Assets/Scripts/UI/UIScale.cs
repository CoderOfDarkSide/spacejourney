﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScale : MonoBehaviour {

	private float lastCamScale = 0;
	public Camera camera;

	void OnEnable()
	{
		if (!camera)
		{
			Debug.Log("Camera is not set");
			enabled = false;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if(lastCamScale != camera.orthographicSize)
		{
			UpdateScale();
		}
	}

	void UpdateScale()
	{
		transform.localScale = new Vector3(camera.orthographicSize / 4, camera.orthographicSize / 8, 1);
	}
}
