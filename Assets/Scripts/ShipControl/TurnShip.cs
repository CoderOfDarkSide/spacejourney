﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnShip : MonoBehaviour {

	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void Update()
	{
		if(transform.rotation.z > 45f)
		{
			transform.Rotate(Vector3.forward * 10f * Time.deltaTime);
		}
		else
		{
			transform.Rotate(Vector3.back * 10f * Time.deltaTime);
		}
	}
}
