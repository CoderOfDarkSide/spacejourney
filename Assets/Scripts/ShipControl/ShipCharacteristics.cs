﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ShipCharacteristics : NetworkBehaviour
{
	[SyncVar]
	public int health = 5000;
	[SyncVar]
	public int maxHealth;
	[SyncVar]
	public int shields = 2550;
	[SyncVar]
	public int maxShields;
	[SyncVar]
	public int oxygen = 100; //percents

	public Image healthBar;
	public Image shieldsBar;
	public Text oxygenText;

	public Text engrHealthText;
	public Image engrHealthBar;
	public Text engrShieldsText;
	public Image engrShieldsBar;

	private void Start()
	{
		maxHealth = health;
		maxShields = shields;
	}

	void Update()
	{
		engrHealthText.text = Convert.ToString(health) + " | " + Convert.ToString(maxHealth);
		engrShieldsText.text = Convert.ToString(shields) + " | " + Convert.ToString(maxShields);
		engrHealthBar.fillAmount = health / 1000f;
		engrShieldsBar.fillAmount = shields / 1000f;

		oxygenText.text = Convert.ToString(oxygen) + "%";
		healthBar.fillAmount = health / 1000f;
		shieldsBar.fillAmount = shields / 1000f;
	}
}
