﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShipEngine : MonoBehaviour
{
    public float torqueForce = 85;
    public float thrustForce = 100;
    public ParticleSystem lEngineParticle;
    public ParticleSystem rEngineParticle;
	Rigidbody2D rb;

	GameObject speedBar;
	Transform canvas;
	GameObject pilotPanel;
	public Image speedBarFill;

	public GameObject ship;
	GameObject shipModel; // Модель корабля

	public float rotateOrbitRadius = 50f; //Радиус вращения вокруг орбиты
	public bool isOnOrbit = false;

    void Start ()
    {
		//Ищем корабль, частицы двигателя и ригидбади
		if(ship == null)
		{
			ship = GameObject.Find("Ship");

			if (ship != null)
			{
				rb = ship.GetComponent<Rigidbody2D>();
				lEngineParticle = ship.transform.Find("LeftEngineParticle").transform.Find("par3").gameObject.GetComponent<ParticleSystem>();
				rEngineParticle = ship.transform.Find("RightEngineParticle").transform.Find("par3").gameObject.GetComponent<ParticleSystem>();
			}
		}

		canvas = GameObject.Find("IngameCanvas").gameObject.transform;
		pilotPanel = canvas.Find("PilotPanel").gameObject;
		speedBar = pilotPanel.transform.Find("SpeedBar").gameObject;
		speedBarFill = speedBar.transform.Find("Image").GetComponent<Image>();
	}

	void Update ()
    {
		//Ищем корабль, ригидбади и модель корабля
		if (ship == null)
		{
			ship = GameObject.Find("Ship");
			if (ship != null)
			{
				rb = ship.GetComponent<Rigidbody2D>();
				shipModel = ship.transform.Find("ShipModel").gameObject;
			}
		}
		if(shipModel == null)
		{
			shipModel = ship.transform.Find("ShipModel").gameObject;
		}

		//По нажатию ЛКМ поворачиваем корабль в сторону курсора
		if (Input.GetKey(KeyCode.Mouse0))
		{
			float tAngle = GetEulerAngle(Camera.main.ScreenToWorldPoint(Input.mousePosition), ship.transform.position);
			TurnShip(torqueForce, tAngle);
		}

        Engine();

		Collider2D[] orbitCollider = Physics2D.OverlapCircleAll(ship.transform.position, rotateOrbitRadius);

		if(orbitCollider.Length > 1)
		{
			Debug.Log(orbitCollider[1].gameObject.name);
			if (orbitCollider[1].gameObject.tag == "Planet")
			{
				ship.transform.RotateAround(orbitCollider[1].transform.position, Vector3.forward, 10f * Time.deltaTime);
				isOnOrbit = true;
			}
		}

		if(Input.GetKeyDown(KeyCode.Z))
		{
			isOnOrbit = false;
		}
	}
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.W))
        {
            OpenThrottle();
        }
        if(Input.GetKey(KeyCode.S))
        {
            CloseThtottle();
        }
    }

    float GetEulerAngle(Vector2 mouseWorldPos, Vector2 currentPos)
    {
        Vector2 direction = mouseWorldPos - currentPos;
        direction.Normalize();
        float angleInWorld = Vector2.Angle(Vector2.up, direction);
        if(direction.x > 0)
        {
            angleInWorld = 360 - angleInWorld;
        }
        return angleInWorld;
    }

    float ConvertAngle(float angle)
    {
        if(angle == 360)
        {
            return 0;
        }
        if(angle > 360)
        {
            return angle - 360;
        }
        if(angle < 0)
        {
            return ConvertAngle(360 + angle);
        }
        return angle;
    }

	//Поворот корабля
    void TurnShip(float torque, float targetAngle)
    {
        float rotLength = targetAngle - ConvertAngle(ship.transform.eulerAngles.z);
        if(rotLength > 0 && rotLength < 180)
        {
			ship.transform.Rotate(Vector3.forward * Time.deltaTime * torque);
			/*if(!(shipModel.transform.rotation.z < 0.045))
			{
				shipModel.transform.Rotate(Vector3.forward);
			}*/
			
		}
        if(rotLength < 0 && rotLength > -180)
        {
			ship.transform.Rotate(Vector3.forward * Time.deltaTime * -torque);
			/*if (!(shipModel.transform.rotation.z < -0.045))
			{
				shipModel.transform.Rotate(Vector3.back);
			}*/
		}
        if(rotLength > 180)
        {
			ship.transform.Rotate(Vector3.forward * Time.deltaTime * -torque);
			
		}
        if(rotLength < -180)
        {
			ship.transform.Rotate(Vector3.forward * Time.deltaTime * torque);
		}
    }
	
	int throttle = 0;
    void OpenThrottle()
    {
        if(throttle < 100)
        {
            throttle++;
        }
    }

    void CloseThtottle()
    {
        if(throttle > 0)
        {
            throttle--;
        }
    }

    void Engine()
    {
		var lParticleMain = lEngineParticle.main;
		var rParticleMain = rEngineParticle.main;
		//lParticleMain.startSpeed = 5 * throttle * 0.01F;
		//rParticleMain.startSpeed = 5 * throttle * 0.01F;
		speedBarFill.fillAmount = (thrustForce * throttle) / 10000;

		rb.AddRelativeForce(Vector2.up * thrustForce * throttle * Time.deltaTime);
    }
}