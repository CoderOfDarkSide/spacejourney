﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AttachToShip : NetworkBehaviour {

	public GameObject Ship;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Ship == null)
		{
			Ship = GameObject.Find("Ship");
			Debug.Log("Finding ship");

			if( Ship != null)
			{
				transform.position = Ship.transform.position; // attach player to ship
				//transform.position = new Vector3(0, 0, 0);
			}
		}
	}
}
