﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAroundOrbit : MonoBehaviour {

	public Transform rotationCenter;
	public float turningSpeed = 1.0f;
	public Vector3 rotationDirection = Vector3.forward;

	Vector3 rotationCenterenterPos;

	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update () {
		rotationCenterenterPos = rotationCenter.transform.position;
		transform.RotateAround(rotationCenterenterPos, rotationDirection, turningSpeed * Time.deltaTime);
	}
}
