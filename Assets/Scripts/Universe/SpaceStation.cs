﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceStation : MonoBehaviour
{
	public float turnSpeed = 1.0f;
	public Vector3 turnDirection = Vector3.forward;

	void Update ()
    {
        transform.Rotate(turnDirection * Time.deltaTime * turnSpeed);
	}
}
