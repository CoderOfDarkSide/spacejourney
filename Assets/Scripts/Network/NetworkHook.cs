﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using Prototype.NetworkLobby;
using UnityEngine;

public class NetworkHook : LobbyHook
{
	public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
	{
		LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
		EnableGui localPlayer = gamePlayer.GetComponent<EnableGui>();

		if(lobby.playerColor == Color.magenta)
		{
			localPlayer.playerRoleName = "Capitan";
		}
		else if (lobby.playerColor == Color.red)
		{
			localPlayer.playerRoleName = "Pilot";
		}
		else if (lobby.playerColor == Color.cyan)
		{
			localPlayer.playerRoleName = "Engeneer";
		}
		else if (lobby.playerColor == Color.blue)
		{
			localPlayer.playerRoleName = "Scientist";
		}
		else if (lobby.playerColor == Color.green)
		{
			localPlayer.playerRoleName = "Weapons";
		}
		else
		{
			localPlayer.playerRoleName = "Capitan";
		}
		base.OnLobbyServerSceneLoadedForPlayer(manager, lobbyPlayer, gamePlayer);
	}
}
